<?php
namespace Maowenke\RankingListForRedis;
class RankingList
{
    protected $redis;
    protected $leaderboard='leaderboard';
    public function __construct($host='127.0.0.1',$port=6379){
        if(empty($redis)){
            $this->redis = new \Redis();
            $this->redis->connect('127.0.0.1');
        }else{
            $this->redis = new \Redis();
            $this->redis->connect($host,$port);
        }
    }
    /**保存数据
     * @param string排行名称 $leaderboard
     * @param string保存的健明 $key
     * @param int分值 $score
     * @return bool
     */
    public function addLeaderboard(string $leaderboard='',string $key,$score=0){
        if(empty($leaderboard)){
            $leaderboard = $this->leaderboard;
        }
        try{
            $this->redis->zAdd($leaderboard, $score, $key);
        }catch (\Exception $e){
            return false;
        }
        return true;
    }

    /**获取排行榜
     * @param string排行名称 $leaderboard
     * @param int获取的数量 $num
     * @param string排序(asc/desc) $sort
     * @param bool否获取分数 $getScore
     * @return array
     */
    public function getLeadboard(string $leaderboard='',int $num=1,string $sort='desc',bool $getScore=false){
        if(empty($leaderboard)){
            $leaderboard = $this->leaderboard;
        }
        if($sort=='desc'){
            $nowLeadboard =  $this->redis->ZREVRANGE($leaderboard, 0, $num -1, $getScore);//按照高分数顺序排行;
        }else{
            $nowLeadboard =  $this->redis->ZRANGE($leaderboard, 0, $num -1, $getScore);//按照低分数顺序排行;
        }
        return $nowLeadboard;
    }

    /**获取某键的排行
     * @param string排行名称 $leaderboard
     * @param string键名 $key
     * @param string排序方法(desc|asc) $sort
     * @return false|int
     */
    public function getKeyRank(string $leaderboard='',string $key,string $sort='desc'){
        if(empty($leaderboard)){
            $leaderboard = $this->leaderboard;
        }
        if($sort=='desc'){
            $res = $this->redis->zRevRank($leaderboard, $key);
        }else{
            $res = $this->redis->zRank($leaderboard, $key);
        }
        return $res;
    }
}